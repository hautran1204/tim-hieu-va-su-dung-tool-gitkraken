## Tìm hiểu và sử dụng GitKraken

### Đầu tiên tạo một repository trên Bitbucket hoặc hosting khác như GitHub.
<img src="img/tạo repo.png">
### Tiếp theo cài đặt GitKraken và thêm profile.
<img src="img/cài đặt và thêm profile.png">
### Trên GitKraken, clone repo mới tạo trên Bitbucket về local. Ở phần "Repository to clone" chọn repo muốn clone về, tại "Where to clone to" tạo đường dẫn lưu repo ở local.
<img src="img/clone repo về local.png">
<img src="img/clone thành công.png">
### Các chức năng:
1. Pull: Cập nhật sự thay đổi từ hosting về local.
2. Push: Sau khi commit sự thay đổi trong repo ở local lên hosting.
3. Branch: Trong repo có thể sẽ có nhiều nhánh và nhánh master sẽ là nhánh chính. các nhánh khác sẽ được merge vào nhánh chính.
4. Commit: Trước khi push sự thay đổi lên hosting, ta cần commit lại sự thay đổi đó một cách chi tiết có thể.

## Sử dụng dòng lệnh
[link tham khảo](https://toituhoc.xyz/git-su-dung-nhanh)